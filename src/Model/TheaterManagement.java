package Model;

import java.util.ArrayList;

public class TheaterManagement {
	private ArrayList<Seat> seats=new ArrayList<Seat>() ;
	
	public TheaterManagement(){
		for(int i = 0; i<15; i++){
			for(int k = 0; k<20; k++){
				seats.add(new Seat(1, (char)(i+65), k, (int)DataSeatPrice.ticketPrices[i][k]));
			}
		}
	}
	
	public void showAll(){
		System.out.print(" ");
		for(int j = 1; j<=20; j++){
			if(j<10){
				System.out.print("  "+j);
			}else{
				System.out.print(" "+j);
			}
		}
		System.out.println("");
		int m=0;
		for(int i = 0; i<15; i++){
			System.out.print((char)(i+65));
			for(int k = 0; k<20; k++){
				System.out.print(" ");
				System.out.print(seats.get(m).getPrice());
				m++;
			}
			System.out.println("");
		}
	}
	
	public int buySeat(int time,char row,int column){
		int price=0;
		for(int i=0;i<seats.size();i++){
			if(seats.get(i).getTime()==time && seats.get(i).getRow()==row &&seats.get(i).getColumn()==column){
				price=seats.get(i).getPrice();
			}
		}
		return price;
	}

	public void buyTbyPrice(int price){
		System.out.print(" ");
		for(int j = 1; j<=20; j++){
			if(j<10){
				System.out.print("  "+j);
			}else{
				System.out.print(" "+j);
			}
		}
		System.out.println("");
		int m=0;
		for(int i = 0; i<15; i++){
			System.out.print((char)(i+65));
			for(int k = 0; k<20; k++){
				System.out.print(" ");
				if(seats.get(m).getPrice()==price){
					System.out.print(seats.get(m).getPrice());
				}else{
					System.out.print("  ");
				}
				m++;
			}
			System.out.println("");
		}
	}
	

}
