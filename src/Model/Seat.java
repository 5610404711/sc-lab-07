package Model;

public class Seat {
	private int time;
	private int price;
	private char row;
	private int column;
	
	public Seat(int time, char row, int column,int price){
		this.time = time;
		this.price = price;
		this.row = row;
		this.column = column;
	}
	
	public int getTime(){
		return time;
	}
	
	public int getPrice(){
		return price;
	}
	
	public char getRow(){
		return row;
	}
	
	public int getColumn(){
		return column;
	}
	
	public void buy(int p){
		this.price=p;
	}
	

}
